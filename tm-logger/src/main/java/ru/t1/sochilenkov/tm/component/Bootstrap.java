package ru.t1.sochilenkov.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.IReceiverService;
import ru.t1.sochilenkov.tm.listener.LoggerListener;
import ru.t1.sochilenkov.tm.service.ReceiverService;

@NoArgsConstructor
public final class Bootstrap {

    @SneakyThrows
    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}
