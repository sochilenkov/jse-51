package ru.t1.sochilenkov.tm.repository.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.model.IProjectRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.model.IUserService;
import ru.t1.sochilenkov.tm.migration.AbstractSchemeTest;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;
import ru.t1.sochilenkov.tm.service.model.UserService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.ProjectConstant.*;

@Category(UnitCategory.class)
public class ProjectRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private IProjectRepository repository;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    public static User USER_1;

    public static User USER_2;

    public static long USER_ID_COUNTER;

    public static EntityManager entityManager;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_1 = userService.create("proj_rep_mod_usr_1_" + USER_ID_COUNTER, "1");
        USER_2 = userService.create("proj_rep_mod_usr_2_" + USER_ID_COUNTER, "1");
        repository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUser(USER_1);
            repository.add(USER_1.getId(), project);
            project.setUser(USER_1);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUser(USER_2);
            repository.add(USER_2.getId(), project);
            project.setUser(USER_2);
            projectList.add(project);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void clearAfter() {
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Test
    public void testAddProjectPositive() {
        Project project = new Project();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        project.setUser(USER_1);
        repository.add(USER_1.getId(), project);
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_1.getId()));
        repository.clear(USER_1.getId());
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));

        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_2.getId()));
        repository.clear(USER_2.getId());
        Assert.assertEquals(0, repository.getSize(USER_2.getId()));
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(USER_1.getId(), UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_2.getId(), UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            final Project foundProject = repository.findOneById(project.getUser().getId(), project.getId());
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(project.getId(), foundProject.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(USER_1.getId(), UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_2.getId(), UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        for (@NotNull final Project project : projectList) {
            Assert.assertTrue(repository.existsById(project.getUser().getId(), project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(USER_1.getId(), 9999));
        Assert.assertNull(repository.findOneByIndex(USER_2.getId(), 9999));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final Project foundProject = repository.findOneByIndex(USER_1.getId(), i + 1);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i).getId(), foundProject.getId());
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            final Project foundProject = repository.findOneByIndex(USER_2.getId(), i + 1);
            Assert.assertNotNull(foundProject);
            Assert.assertEquals(projectList.get(i + 5).getId(), foundProject.getId());
        }
    }

    @Test
    public void testFindAll() {
        @NotNull List<Project> projects = repository.findAll(USER_1.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projects.get(i).getId(), projectList.get(i).getId());
        }
        projects = repository.findAll(USER_2.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projects.get(i - 5).getId(), projectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllOrderCreated() {
        List<Project> projects = repository.findAll(USER_1.getId(), CREATED_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_1.getId())) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_2.getId(), CREATED_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_2.getId())) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderStatus() {
        List<Project> projects = repository.findAll(USER_1.getId(), STATUS_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_1.getId())) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_2.getId(), STATUS_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_2.getId())) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testFindAllOrderName() {
        List<Project> projects = repository.findAll(USER_1.getId(), NAME_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_1.getId())) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        projects = repository.findAll(USER_2.getId(), NAME_SORT);
        Assert.assertNotNull(projects);
        Assert.assertEquals(INIT_COUNT_PROJECTS, projects.size());
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_2.getId())) {
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getId().equals(m.getId()))
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_1.getId()));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            repository.removeById(USER_1.getId(), projectList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_1.getId(), projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_2.getId()));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            repository.removeById(USER_2.getId(), projectList.get(i).getId());
            Assert.assertNull(repository.findOneById(USER_2.getId(), projectList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_2.getId()));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_1.getId()));
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            repository.remove(USER_1.getId(), projectList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_1.getId()));
        Assert.assertEquals(INIT_COUNT_PROJECTS, repository.getSize(USER_2.getId()));
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            repository.remove(USER_2.getId(), projectList.get(i));
        }
        Assert.assertEquals(0, repository.getSize(USER_2.getId()));
    }

}
