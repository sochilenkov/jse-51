package ru.t1.sochilenkov.tm.service.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.sochilenkov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.dto.IUserDtoService;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.TaskIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;
import ru.t1.sochilenkov.tm.dto.model.TaskDTO;
import ru.t1.sochilenkov.tm.migration.AbstractSchemeTest;
import ru.t1.sochilenkov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.sochilenkov.tm.repository.dto.TaskDtoRepository;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.ProjectTaskConstant.*;

@Category(UnitCategory.class)
public class ProjectTaskDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private static IProjectDtoRepository projectRepository;

    @NotNull
    private static ITaskDtoRepository taskRepository;

    private static EntityManager entityManager;

    @NotNull
    private static IProjectTaskDtoService projectTaskService;

    public static String USER_ID_1;

    public static String USER_ID_2;

    @NotNull
    private static IUserDtoService userService;

    public static long USER_ID_COUNTER = 0;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private List<TaskDTO> taskList;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskDtoService(connectionService);
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);
        taskRepository = new TaskDtoRepository(entityManager);
        userService = new UserDtoService(propertyService, connectionService);
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = userService.create("proj_tsk_serv_mod_usr_1_" + USER_ID_COUNTER, "1").getId();
        USER_ID_2 = userService.create("proj_tsk_serv_mod_usr_2_" + USER_ID_COUNTER, "1").getId();
        entityManager.getTransaction().begin();
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project");
            project.setDescription("Description");
            project.setUserId(i == 1 ? USER_ID_1 : USER_ID_2);
            projectRepository.add(project.getUserId(), project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_1);
            taskRepository.add(USER_ID_1, task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_2);
            taskRepository.add(USER_ID_2, task);
            taskList.add(task);
        }
        entityManager.getTransaction().commit();
    }

    @After
    public void closeConnection() {
        entityManager.getTransaction().begin();
        taskRepository.clear(USER_ID_1);
        taskRepository.clear(USER_ID_2);
        projectRepository.clear(USER_ID_1);
        projectRepository.clear(USER_ID_2);
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @Test
    public void testBindTaskToProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() {
        for (final ProjectDTO project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final TaskDTO task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    projectTaskService.bindTaskToProject(project.getUserId(), project.getId(), task.getId());
            }
            List<TaskDTO> tasks = taskRepository.findAll(project.getUserId());
            for (final TaskDTO task : tasks) {
                Assert.assertNotNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() {
        testBindTaskToProjectPositive();
        for (final ProjectDTO project : projectList) {
            Assert.assertNotEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final TaskDTO task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    projectTaskService.unbindTaskFromProject(project.getUserId(), project.getId(), task.getId());
            }
            List<TaskDTO> tasks = taskRepository.findAll(project.getUserId());
            for (final TaskDTO task : tasks) {
                Assert.assertNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() {
        testBindTaskToProjectPositive();
        for (final ProjectDTO project : projectList) {
            Assert.assertNotNull(taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(INIT_COUNT_TASKS, taskRepository.findAll(project.getUserId()).size());
            projectTaskService.removeProjectById(project.getUserId(), project.getId());
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(0, taskRepository.findAll(project.getUserId()).size());
        }
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_1) + taskRepository.getSize(USER_ID_2));
    }

}
