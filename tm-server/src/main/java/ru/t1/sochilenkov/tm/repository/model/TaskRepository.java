package ru.t1.sochilenkov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.repository.model.ITaskRepository;
import ru.t1.sochilenkov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<Task> getEntityClass() {
        return Task.class;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(query, getEntityClass()).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

}
