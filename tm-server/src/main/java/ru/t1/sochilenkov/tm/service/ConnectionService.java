package ru.t1.sochilenkov.tm.service;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;
import ru.t1.sochilenkov.tm.dto.model.SessionDTO;
import ru.t1.sochilenkov.tm.dto.model.TaskDTO;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEMFactory();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEMFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl() + propertyService.getDBSchema());
        settings.put(Environment.USER, propertyService.getDBUser());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.FORMAT_SQL, "true");
        settings.put(Environment.SHOW_SQL, propertyService.getDBShowSQL());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2DDL());
        if ("true".equals(propertyService.getDBL2Cache())) {
            settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBL2Cache());
            settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBCacheRegion());
            settings.put(Environment.USE_QUERY_CACHE, propertyService.getDBQueryCache());
            settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBMinimalPuts());
            settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBCacheRegionPrefix());
            settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBCacheProvider());
        }
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
