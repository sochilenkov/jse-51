package ru.t1.sochilenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.request.TaskStartByIdRequest;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    public static final String NAME = "task-start-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken());
        request.setId(id);

        getTaskEndpoint().startTaskById(request);
    }

}
