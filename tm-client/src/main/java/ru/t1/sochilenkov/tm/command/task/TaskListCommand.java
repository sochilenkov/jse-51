package ru.t1.sochilenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.model.TaskDTO;
import ru.t1.sochilenkov.tm.dto.request.TaskListRequest;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show all tasks.";

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);

        @Nullable final List<TaskDTO> tasks = getTaskEndpoint().listTask(request).getTasks();
        if (tasks != null) renderTasks(tasks);
    }

}
