package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull final UserDTO user) {
        super(user);
    }

}
