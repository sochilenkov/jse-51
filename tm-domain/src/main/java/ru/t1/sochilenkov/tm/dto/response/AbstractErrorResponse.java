package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public abstract class AbstractErrorResponse extends AbstractResultResponse {

    public AbstractErrorResponse() {
        setSuccess(false);
    }

    public AbstractErrorResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }
}
